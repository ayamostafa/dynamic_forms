import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormTemplate } from '../models/form-template';
const BASE_URL = 'http://localhost:3000/';

@Injectable({
  providedIn: 'root'
})
export class FormTemplateService {

  model = 'formtemplates';

  constructor(private httpClient: HttpClient) { }

  //Service that consume the json structure from Api, or user input and transform it to form group
  toFormGroup(formTemplates: FormTemplate[]) {
    const group: any = {};
    formTemplates.forEach((template) => {
      group[template.key] = template.is_required ? new FormControl(template.value || '', Validators.required) : new FormControl(template.value || '');
    });
    return new FormGroup(group);
  }
  //If get form template from local server
  getUrl() {
    return `${BASE_URL}${this.model}`;
  }
  getUrlForId(id) {
    return `${this.getUrl()}/${id}`;
  }
  all() {
    return this.httpClient.get<FormTemplate[]>(this.getUrl());
  }


}
