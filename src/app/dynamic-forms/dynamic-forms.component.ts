import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { FormTemplate } from '../models/form-template';
import { FormTemplateService } from '../services/form-template.service';
import { JsonValidatorService } from '../services/json-validator.service';

enum DataSource {
  Api,
  User
}
@Component({
  selector: 'app-dynamic-forms',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.scss']
})
export class DynamicFormsComponent implements OnInit {
  formtemplates;
  userInput = "";
  jsonerrors;
  dynamicForm: FormGroup;
  transUserInputJson: FormTemplate[];
  dataSoure = DataSource;

  constructor(private formTemplatesApi: FormTemplateService, private _snackBar: MatSnackBar, private jsonValidator: JsonValidatorService) {
  }
  ngOnInit(): void {
  }

  renderJson(source) {
    this.jsonerrors = undefined;
    if (source == DataSource.Api) // if  json from REST Api
    {
      this.getFormTemplate();
    }
    else if (source == DataSource.User) // if  json from user input
    {
      if (this.userInput !== "") {
        let transUserInput = this.jsonValidator.parseJSONSafely(this.userInput);
        Object.keys(transUserInput).length > 0 ?
          this.transformJsonToForm(transUserInput) :
          this.ShowErrMsg("Please, Make sure that data in correct json format.");
      } else {
        this.ShowErrMsg("Please, Insert Json Object in textarea.");
      }
    }
  }

  ShowErrMsg(msg, close?: boolean) {
    if (close) {
      this._snackBar.open(msg, "Cancel", {
        duration: 2000
      });
    } else {
      this._snackBar.open(msg, "Cancel");
    }
  }
  public async getFormTemplate() {
    await this.formTemplatesApi.all().toPromise().then(result => {
      let data = {};
      data["formtemplates"] = result;
      this.transformJsonToForm(data);
    }).catch(error => {
      this.ShowErrMsg("Please, Make sure you run this command in your terminal: json-server --watch db.json");
    });

  }
  transformJsonToForm(jsonData) {
    if (this.jsonValidator.validateJson(jsonData)) { // if json in valid format
      this.formtemplates = jsonData.formtemplates;
      this.dynamicForm = this.formTemplatesApi.toFormGroup(this.formtemplates);
    } else {
      this.jsonerrors = this.jsonValidator.getJsonErrors();
      this.ShowErrMsg("Please, Make sure that data in correct json format.", true);
    }
  }

}
