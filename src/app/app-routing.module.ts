import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DynamicFormsComponent } from './dynamic-forms/dynamic-forms.component';


const routes: Routes = [
  { path: 'dynamic-forms', component: DynamicFormsComponent },
  {
    path: '**',
    component: DynamicFormsComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
