import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormTemplate } from 'src/app/models/form-template';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.scss']
})
export class FormTemplateComponent implements OnInit {

  @Input() formTemplate: FormTemplate;
  @Input() formGroup: FormGroup;
  constructor() { }

  ngOnInit(): void {
  }

  get isValid() { return this.formGroup.controls[this.formTemplate.key].valid; }

}
