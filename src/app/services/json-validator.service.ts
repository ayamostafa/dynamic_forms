import { Injectable } from '@angular/core';
import * as Ajv from 'ajv';

@Injectable({
  providedIn: 'root'
})
export class JsonValidatorService {
  ajv;
  schema;
  constructor() {
    this.schema =
    {
      "type": "object",
      "properties": {
        "formtemplates": {
          "type": "array",
          "items":
          {
            "type": "object",
            "properties": {
              "controlType": { "type": "string" },
              "name": { "type": "string" },
              "input_type": { "type": "string" },
              "value": { "type": "string" },
              "key": { "type": "string" },
              "label": { "type": "string" },
              "is_required": { "type": "boolean" },
              "drop_down_items": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "key": { "type": "string" },
                    "value": { "type": "string" },
                  },
                  "required": ["key", "value"],
                }

              }
            },
            "required": ["controlType", "name"],
            "additionalProperties": false
          }
        }
      }
    }

  }
  validateJson(jsonData) {
    this.ajv = Ajv({ allErrors: true });

    let isValidJson = this.ajv.validate(this.schema, jsonData);
    return isValidJson;
  }
  getJsonErrors() {
    return this.ajv.errorsText(this.ajv.errors);
  }
  parseJSONSafely(data) {
    try {
      return JSON.parse(data);
    }
    catch (e) {
      return {}
    }
  }
}
