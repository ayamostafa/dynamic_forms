export class FormTemplate {
  controlType: string; // for example (input, textarea, select...)
  name: string;  //  name is used for form submission
  input_type?: string; // in case of control type was input then input_type may be (text, email, tel, password...
  value?: string;   // value of form control if found
  key: string;   // key is used for id of form control and naming for form controls
  label?: string; // label
  is_required?: boolean;
  drop_down_items?: { key: string, value: string }[];

  constructor(options: {
    controlType: string;
    name: string;
    input_type?: string;
    value?: string;
    key: string;
    label?: string;
    is_required?: boolean;
    drop_down_items?: { key: string, value: string }[];
  }) {

    this.controlType = options.controlType;
    this.name = options.name;
    this.input_type = options.input_type || '';
    this.value = options.value || '';
    this.key = options.key;
    this.label = options.label || '';
    this.is_required = options.is_required || false;
    this.drop_down_items = options.drop_down_items;

  }
}
